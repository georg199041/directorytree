const fs = require('fs');
const path = require('path');

class Abstract {
    constructor() {
    }

    get() {
    }
}

class ES6Style extends Abstract {
    get(url, cb) {
        getAsync(url, cb);
    }
}

/**
 * Reads recursively directory tree, collects result and pass it in cb function
 * @param url:string
 * @param cb:Function
 */
function getAsync(url, cb) {
    const result = {
        filenames: [],
        dirnames: []
    };
    const readTree = function(url) {
        if (!~result.dirnames.indexOf(url)) {
            result.dirnames.push(url.replace('/', ''));
        }
        fs.readdir(url, function(err, files) {
            if (files && !files.length) {
                cb(null, result);
            } else {
                files.forEach(function(item) {
                    const nextPath = path.join(url, item);
                    fs.lstat(nextPath, function(err, data) {
                        if (!err && data.isDirectory()) {
                            result.dirnames.push(nextPath);
                            readTree(nextPath);
                        } else {
                            result.filenames.push(nextPath);
                        }
                    });

                });
            }
        });
    };
    readTree(url);
}

module.exports = {
    getAsync: getAsync,
    ES6Style: ES6Style
};
