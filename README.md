Another directory tree structure reader 

`npm install`  
`node example "foo/"`  
`npm run test`

basic usage : 
```
const directoryContent = require('./index');
const DirectoryContent = directoryContent.ES6Style;
const directoryContent2 = new DirectoryContent();

/**
 * Async example how to use
 */
directoryContent.getAsync(process.argv[2], function(err, result) {
    console.log(result);
});

/**
 * Same async example, but in ES6 style
 */
directoryContent2.get(process.argv[2], function(err, result) {
    console.log(result);
});
```