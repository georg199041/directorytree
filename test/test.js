var assert = require('chai').assert;
const directoryContent = require('../index');
const DirectoryContent = directoryContent.ES6Style;

describe('Async directory read', function() {
    var objToCompare = {
        filenames: [
            'foo/f1.txt',
            'foo/f2.txt',
            'foo/bar/bar1.txt',
            'foo/bar/bar2.txt'
        ],
        dirnames: [ 'foo', 'foo/bar', 'foo/bar/baz' ]
    };
    it('should return object for async function', function(done) {
        directoryContent.getAsync('foo/', function(err, result) {
            assert.property(result, 'filenames');
            assert.property(result, 'dirnames');
            objToCompare.filenames.forEach(function(item) {
                assert.isTrue(!!~result.filenames.indexOf(item));
            });
            objToCompare.dirnames.forEach(function(item) {
                assert.isFalse(!~result.dirnames.indexOf(item));
            });
            done();
        });
    });
    it('should return object for ES6 class', function(done) {
        const directoryContent2 = new DirectoryContent();
        directoryContent2.get('foo/', function(err, result) {
            // assert.deepEqual(result, objToCompare);
            assert.property(result, 'filenames');
            assert.property(result, 'dirnames');
            objToCompare.filenames.forEach(function(item) {
                assert.isTrue(!!~result.filenames.indexOf(item));
            });
            objToCompare.dirnames.forEach(function(item) {
                assert.isFalse(!~result.dirnames.indexOf(item));
            });
            done();
        });
    });
});